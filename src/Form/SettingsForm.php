<?php

namespace Drupal\yandex_translator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a form that configures yandex_translator settings.
 */
class SettingsForm extends ConfigFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_translator_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'yandex_translator.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $devel_config = $this->config('yandex_translator.settings');
    $register_app = 'https://tech.yandex.com/translate/';

    $form['api_key'] = [
      '#type' => 'checkbox',
      '#title' => t('Display $page array'),
      '#default_value' => $devel_config->get('api_key'),
      '#description' =>  t('Please enter the API KEY, or follow this <a href=":link">link</a> to set it up.', array(':link' => $register_app)),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    //    * TODO : Validate the key : $values['api_key']

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('yandex_translator.settings')
      ->set('api_key', $values['api_key'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
