<?php

namespace Drupal\yandex_translator\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Edit config variable form.
 */
class YandexTranslator extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'yandex_translator_translate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config_name = '') {

    $form['input'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text to translate'),
      '#default_value' => '',
      '#rows' => 5,
      '#required' => TRUE,
    ];

    $form['output'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Translation'),
      '#rows' => 5,
    ];

    $form['actions'] = [
      '#type' => 'submit',
      '#title' => $this->t('Translate'),

      // TODO : add state : visible
    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getValue('input');
    if(!$input){
      $form_state->setErrorByName('input','Input text is empty');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getValue('input');
    try {

      //TODO / Translate
    }
    catch (\Exception $e) {
      drupal_set_message($e->getMessage(), 'error');

    }

    $form_state->setRebuild(TRUE);
  }



}
